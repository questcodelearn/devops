#!/bin/sh
curl -LO https://git.io/get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh

kubectl apply -f /mnt/devops/07-deploys-rollbacks-helm/01-tiller-account.yaml

helm init --service-account tiller

# helm init
# kubectl patch deployment tiller-deploy -n kube-system --patch "$(cat /mnt/devops/07-deploys-rollbacks-helm/01-tiller-patch.yaml)"