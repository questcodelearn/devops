#!/bin/sh
# Conforme Executado no Bootcamp
# Missao 05 > Orquestracao e Kubernetes > Configurando Client (kubectl)

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config